package mx.edu.utez.peta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetaApplication.class, args);
	}

}
